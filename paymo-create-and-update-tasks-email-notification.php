<?php
$paymo_integration = get_option( 'paymo-checkbox' );
if($paymo_integration == 1) {

  function paymo_create_and_update_task($post_id) {
    $get_post = get_post($post_id);
    $post_status = $get_post->post_status;
    $author = $get_post->post_author;

    if ( ('support' == $get_post->post_type) && ($post_status != 'trash') ) {

      $api = get_option( 'paymo-input-api' );
      $projectname = get_option( 'paymo-input-projectname' );

      //get---------------------------------------------------------------------
      $get_args = array(
        'timeout'     => 5,
        'redirection' => 5,
        'httpversion' => '1.0',
        'user-agent'  => 'WordPress/' . $wp_version . '; ' . get_bloginfo( 'url' ),
        'blocking'    => true,
        'headers'     => array('Authorization' => 'Basic ' . base64_encode($api . ':' . 'X')),
        'cookies'     => array(),
        'body'        => array(),
        'compress'    => false,
        'decompress'  => true,
        'sslverify'   => true,
        'stream'      => false,
        'filename'    => null
      );

      //Getting projects
      $url_projects = 'https://app.paymoapp.com/api/projects';
      $response_projects = wp_remote_get($url_projects, $get_args);
      $get_projects = json_decode($response_projects['body']);
      //--------------------------------------------------------------------------

      foreach ($get_projects->projects as $project) {
        if($projectname == $project->name){
          $project_id = $project->id;
        }
      }

      $post_title = get_the_title( $post_id );
      $content = $get_post->post_content;
      $user = wp_get_current_user();
      $current_user = $user->user_login;
      $edit_post_link = get_edit_post_link ($post_id);
      $user_id = $get_post->post_author;
      $email_notification = get_user_meta( $user_id, '_email_input_checkbox', true );

      if ($email_notification == 1) {
        $user_id = $get_post->post_author;
        global $wpdb;
        $user_info = $wpdb->get_results("SELECT user_email FROM $wpdb->users WHERE ID = $user_id");
        $user_email = $user_info[0]->user_email;
      }

      $administrators_info = get_users( array('role' => 'Administrator') );
      $email_administrators = array();
      foreach ( $administrators_info as $user ) {
        array_push($email_administrators, $user->user_email);
      }

      $admin_emails = implode(",", $email_administrators);
      $message_to = $user_email . ',' . $admin_emails;

      $priority = get_post_meta( $post_id, '_priority_meta_value_key', true );
      $url_of_the_problem_page = get_post_meta( $post_id, '_url_meta_value_key', true );
      $status = get_post_meta( $post_id, '_status_meta_value_key', true );
      $browser = get_post_meta( $post_id, '_browser', true );

      $url_tasks = 'https://app.paymoapp.com/api/tasks?where=project_id=' . $project_id;
      $url_tasklists = 'https://app.paymoapp.com/api/tasklists?where=project_id=' . $project_id;


      //Getting tasks
      $response_tasks = wp_remote_get($url_tasks, $get_args);
      $get_tasks = json_decode($response_tasks['body']);
      // end

      //Getting task lists
      $response_tasklists = wp_remote_get($url_tasklists, $get_args);
      $get_tasklists = json_decode($response_tasklists['body']);
      // end

      //Geting task list id
      foreach ($get_tasklists->tasklists as $tasklist) {
        if($tasklist->name == 'Low') {
          $low = $tasklist->id;
        }
        if($tasklist->name == 'Medium') {
          $medium = $tasklist->id;
        }
        if($tasklist->name == 'High') {
          $high = $tasklist->id;
        }
      }
      // end

      //end get-----------------------------------------------------------------

      $first_comment = '<b>More for ticket</b>' . '<br>' . '<a href="' . $url_of_the_problem_page .
                       '" target="_blank">URL of the problem page</a>' .
                       '<br>' . 'User in WordPress: '. $current_user . '<br>' .
                       '<a href="' . $edit_post_link .
                       '" target="_blank">URL of the ticket in WordPress</a>' . '<br>' .
                       'Browser ' . $browser;

      function paymo_crate_task($post_id, $post_title, $content, $priorities, $api, $first_comment, $message_to) {
        $task_body = array(
          'name' => $post_title,
          'description' => $content,
          'tasklist_id' => $priorities
        );

        $args_task = array(
          'method' => 'POST',
          'timeout' => 45,
          'redirection' => 5,
          'httpversion' => '1.0',
          'blocking' => true,
          'headers' => array('Authorization' => 'Basic ' . base64_encode($api . ':' . 'X')),
          'body' => $task_body,
          'cookies' => array()
        );

        $url_tasks = 'https://app.paymoapp.com/api/tasks';
        $tasks = wp_remote_post( $url_tasks, $args_task);
        $get_task_id = json_decode($tasks['body']);

        foreach ($get_task_id->tasks as $task) {
          $task_id= $task->id;
        }

        add_post_meta($post_id, 'task_id', $task_id);

        $to =  $message_to;
        $subject =  'Ceate a ticket with title: ' . $post_title;
        $message = 'Created a new ticket with a name ' . $post_title . '.';
        wp_mail( $to, $subject, $message );

        $args_comment = array(
          'method' => 'POST',
          'timeout' => 45,
          'redirection' => 5,
          'httpversion' => '1.0',
          'blocking' => true,
          'headers' => array('Authorization' => 'Basic ' . base64_encode($api . ':' . 'X')),
          'body' => array('task_id' => $task_id, 'content' => $first_comment),
          'cookies' => array()
        );
        $url_comments = 'https://app.paymoapp.com/api/comments';
        $comment =  wp_remote_post( $url_comments, $args_comment);
      }


      $task_id = get_post_meta( $post_id, 'task_id', true );

      foreach ($get_tasks->tasks as $task) {
        if($task->id == $task_id) {
          $have_task = 1;

          //update task
          if($task->name != $post_title) {
            $update_name = $post_title;

            $to =  $message_to;
            $subject = 'Edited ticket with title: ' . $post_title;
            $message = 'The title of the ticket with the previous title ' .
                        $task->name . ' was changed to ' . $post_title . '.';
            wp_mail( $to, $subject, $message );
          }

          $clean_paymo_description = preg_replace('/[^A-Za-z0-9 !@#$%^&*().]/u','', strip_tags($task->description));
          $clean_wp_content = preg_replace('/[^A-Za-z0-9 !@#$%^&*().]/u','', strip_tags($content));

          if($clean_paymo_description != $clean_wp_content) {
            $update_description = $content;

            $to =  $message_to;
            $subject =  'Edited ticket with title: ' . $post_title;
            $message = 'The content of the ticket with title ' . $post_title . ' was edited. New content is: ' . $content;
            wp_mail( $to, $subject, $message );
          }

          $update_priority = $priority;
          if($update_priority == 'Low') {
            $tasklist_id = $low;
          }

          if($update_priority == 'Medium') {
            $tasklist_id = $medium;
          }

          if($update_priority == 'High') {
            $tasklist_id = $high;
          }

          if($task->tasklist_id != $tasklist_id){
            if($tasklist_id == $low){
              $to =  $message_to;
              $subject = 'Edited ticket with title: ' . $post_title;
              $message = 'The priority of the ticket with title ' . $post_title . ' was edited. New priority is: Low.';
              wp_mail( $to, $subject, $message );
            }

            if($tasklist_id == $medium){
              $to =  $message_to;
              $subject = 'Edited ticket with title: ' . $post_title;
              $message = 'The priority of the ticket with title ' . $post_title . ' was edited. New priority is: Medium.';
              wp_mail( $to, $subject, $message );
            }

            if($tasklist_id == $high){
              $to =  $message_to;
              $subject = 'Edited ticket with title: ' . $post_title;
              $message = 'The priority of the ticket with title ' . $post_title . ' was edited. New priority is: High.';
              wp_mail( $to, $subject, $message );
            }
          }

          if($task->complete != $status) {
            if ($status == 1) {
              $to =  $message_to;
              $subject = 'Edited ticket with title: ' . $post_title;
              $message = 'The status of the ticket with title ' . $post_title . ' was edited. New status is: Solved.';
              wp_mail( $to, $subject, $message );
            }

            if ($status != 1) {
              $to =  $message_to;
              $subject =  'Task edited';
              $message = 'The status of the ticket with title ' . $post_title . ' was edited. New status is: Not Solved.';
              wp_mail( $to, $subject, $message );
            }
          }

          $task_body = array(
            'name' => $update_name,
            'description' => $update_description,
            'complete' => $status,
            'tasklist_id' => $tasklist_id
          );

          $args_task = array(
            'method' => 'POST',
            'timeout' => 45,
            'redirection' => 5,
            'httpversion' => '1.0',
            'blocking' => true,
            'headers' => array('Authorization' => 'Basic ' . base64_encode($api . ':' . 'X')),
            'body' => $task_body,
            'cookies' => array()
          );

          $url_tasks_id = 'https://app.paymoapp.com/api/tasks/' . $task->id;
          $tasks = wp_remote_post( $url_tasks_id, $args_task);

        }
      }

      if($have_task != 1) {
        if($priority == 'Low') {
          paymo_crate_task($post_id, $post_title, $content, $low,  $api, $first_comment, $message_to);
        }
        if($priority == 'Medium') {
          paymo_crate_task($post_id, $post_title, $content, $medium,  $api, $first_comment, $message_to);
        }
        if($priority == 'High') {
          paymo_crate_task($post_id, $post_title, $content, $high,  $api, $first_comment, $message_to);
        }
      }
    }
  }
}
add_action( 'save_post', 'paymo_create_and_update_task', 999 );
?>
