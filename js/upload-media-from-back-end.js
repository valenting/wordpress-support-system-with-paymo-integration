jQuery(function($) {
  $(document).ready(function(){
    $('#upload-media').click(open_media_window);
  });

  function open_media_window() {
    var frame;
    if (frame) {
      mediaWindow.open();
    }

    mediaWindow = wp.media({
      title: 'Insert Media',
      multiple: false,
      button: {text: 'Insert'}
    });

    mediaWindow.on('select', function() {
      var media = mediaWindow.state().get('selection').first().toJSON();

      var isNotEmpty = $("#id-media-upload").val();
      if( isNotEmpty ) {
        $("#id-media-upload").val(isNotEmpty + "," + media.id);
      } else {
        $("#id-media-upload").val(media.id);
      }

      if(media.type == 'image') {
        $( ".before-save-post" ).append(
          '<ul class="media-list">' +
            '<li>' +
              '<div class="media-thumbnail">' +
                '<a href="'+ media.url +'" target="_blank">' +
                  '<div class="media-img-image">' +
                    '<img src=" ' + media.sizes.thumbnail.url + ' " alt="" />' +
                  '</div>' +
                  '<span class="media-name"><span>'+  media.name +'</span></span>' +
                '</a>' +
              '</div>' +
            '</li>' +
            '</ul>'
        );
      }

      if(media.type == 'video') {
        $( ".before-save-post" ).append(
          '<ul class="media-list">' +
            '<li>' +
              '<div class="media-thumbnail">' +
                '<a href="'+ media.url +'" target="_blank">' +
                  '<div class="media-img">' +
                    '<img src=" ' + img.video + ' " alt="" />' +
                  '</div>' +
                  '<span class="media-name"><span>'+ media.name +'</span></span>' +
                '</a>' +
              '</div>'+
            '</li>' +
          '</ul>'
        );
      }

      if(media.type == 'text') {
        $( ".before-save-post" ).append(
          '<ul class="media-list">' +
            '<li>' +
              '<div class="media-thumbnail">' +
                '<a href="'+ media.url +'" target="_blank">' +
                  '<div class="media-img">' +
                    '<img src=" ' + img.text + ' " alt="" />' +
                  '</div>' +
                  '<span class="media-name"><span>'+ media.name +'</span></span>' +
                '</a>' +
              '</div>'+
            '</li>' +
          '</ul>'
        );
      }

      if(media.type == 'application') {
        $( ".before-save-post" ).append(
          '<ul class="media-list">' +
            '<li>' +
              '<div class="media-thumbnail">' +
                '<a href="'+ media.url +'" target="_blank">' +
                  '<div class="media-img">' +
                    '<img src=" ' + img.application + ' " alt="" />' +
                  '</div>' +
                  '<span class="media-name"><span>'+ media.name +'</span></span>' +
                '</a>' +
              '</div>' +
            '</li>' +
          '</ul>'
        );
      }

      if((media.type == 'audio') ) {
        $( ".before-save-post" ).append(
          '<ul class="media-list">' +
            '<li>' +
              '<div class="media-thumbnail">' +
                '<a href="'+ media.url +'" target="_blank">' +
                  '<div class="media-img">' +
                    '<img src=" ' + img.audio + ' " alt="" />' +
                  '</div>' +
                  '<span class="media-name"><span>'+ media.name +'</span></span>' +
                '</a>' +
              '</div>'+
            '</li>' +
          '</ul>'
        );
      }
    });
    mediaWindow.open();
  }
});
