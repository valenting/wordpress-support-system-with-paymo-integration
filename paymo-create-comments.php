<?php
function paymo_create_comments( $comment_ID, $post_id) {
  $api = get_option( 'paymo-input-api' );
  $comment = get_comment( $comment_ID );

  $comment_comment = $comment->comment_content;
  $post_id = $comment->comment_post_ID;

  $task_id = get_post_meta( $post_id, 'task_id', true );

  $args_comment = array(
    'method' => 'POST',
    'timeout' => 45,
    'redirection' => 5,
    'httpversion' => '1.0',
    'blocking' => true,
    'headers' => array('Authorization' => 'Basic ' . base64_encode($api . ':' . 'X')),
    'body' => array('task_id' => $task_id, 'content' => $comment_comment),
    'cookies' => array()
  );

  $url_comments = 'https://app.paymoapp.com/api/comments';
  $comment =  wp_remote_post( $url_comments, $args_comment);
  $get_comment_id = json_decode($comment['body']);

  foreach ($get_comment_id->comments as $comment) {
    $paymo_wp_comment_id= $comment->id;
  }
  add_comment_meta( $comment_ID, 'comment_id', $paymo_wp_comment_id );
}
add_action( 'comment_post', 'paymo_create_comments');
?>
