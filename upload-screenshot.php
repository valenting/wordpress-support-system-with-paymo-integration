<?php
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );

$decoded = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $_POST['screenshot']));

$time = current_time('d-m-Y_H-i-s');
$the_filename = $time . '_screenshot.png';

file_put_contents( $the_filename, $decoded );

if( !function_exists( 'wp_handle_sideload' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/file.php' );
}

$temp_file =   $the_filename;
$file = array(
	'name' => $the_filename,
	'type' => 'image/png',
	'tmp_name' => $temp_file,
	'error' => 0,
	'size' => filesize( $temp_file )
);

$results = wp_handle_sideload( $file, array( 'test_form' => false ) );

if (!empty($results['error'])) {
	echo "Error!";
} else {
	$filename = $results['file'];
	$wp_upload_dir = wp_upload_dir();
	$attachment = array(
		'guid' => $wp_upload_dir['url'] . '/' . basename($filename),
		'post_mime_type' => $results['type'],
		'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
		'post_content' => '',
		'post_status' => 'inherit'
	);

	$attach_id = wp_insert_attachment( $attachment, $filename, 0 );
	echo $attach_id;

	require_once(ABSPATH . 'wp-admin/includes/image.php');

	$attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
	wp_update_attachment_metadata( $attach_id, $attach_data );

	set_post_thumbnail( 0, $attach_id );
}
?>
