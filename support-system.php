<?php
/*
Plugin Name: WordPress Support System with Paymo Integration
Description: System that allows support of websites based on WordPress. The system has integration with the platform for project management Paymo.
Author: Valentin Georgiev
*/

include 'create-custom-post-type-support.php';
include 'add-meta-boxes.php';
include 'add-custom-column-tickets-list.php';
include 'admin-bar-ticket.php';
include 'form-create-ticket-admin-bar.php';
include 'add-settings-page.php';
include 'paymo-create-task-lists.php';
include 'paymo-create-and-update-tasks-email-notification.php';
include 'paymo-delete-tasks.php';
include 'paymo-create-comments.php';
include 'paymo-edit-comments.php';
include 'paymo-delete-comments.php';

function register_scripts_and_styles() {
  wp_register_script( 'js_upload_media_from_back_end', plugins_url('/js/upload-media-from-back-end.js',__FILE__));
  $images = array(
    'video' => plugins_url("img/video.png",__FILE__),
    'text' => plugins_url("img/text.png",__FILE__),
    'application' => plugins_url("img/application.png",__FILE__),
    'audio' => plugins_url("img/audio.png",__FILE__),
  );
  wp_localize_script( 'js_upload_media_from_back_end', 'img', $images );

  wp_register_script( 'create_ticket_admin_bar', plugins_url('/js/create-ticket-admin-bar.js',__FILE__));
  wp_register_script( 'html2canvas', plugins_url('/js/html2canvas.js',__FILE__));
  wp_register_script( 'jquery_plugin_html2canvas', plugins_url('/js/jquery.plugin.html2canvas.js',__FILE__));
  wp_register_style( 'style_support_system', plugins_url('/css/style-support-system.css',__FILE__));
  wp_enqueue_style('style_support_system');
}
add_action('admin_enqueue_scripts', 'register_scripts_and_styles');
add_action('wp_enqueue_scripts', 'register_scripts_and_styles');

//Translate
add_action('plugins_loaded', 'support_load_textdomain');
function support_load_textdomain() {
	load_plugin_textdomain( 'support', false, dirname( plugin_basename(__FILE__) ) . '/language/' );
}
?>
