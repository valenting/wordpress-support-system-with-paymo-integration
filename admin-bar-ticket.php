<?php
add_action('admin_bar_menu', 'add_new_ticke_admin_bar', 999);
function add_new_ticke_admin_bar ($wp_admin_bar) {
  $args = array(
    'id' => 'ticket-button',
    'title' => __( 'Add New Ticket', 'support' ),
    'meta' => array(
      'class' => 'create-ticket'
    )
  );
  $wp_admin_bar->add_node($args);
}

add_action( 'admin_bar_menu', 'remove_node_new_ticket',999);
function remove_node_new_ticket( $wp_admin_bar ) {
  $wp_admin_bar->remove_node( 'new-support' );
}
?>
