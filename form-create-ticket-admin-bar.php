<?php
function create_ticket_admin_bar_ticket() {
  wp_enqueue_script('create_ticket_admin_bar');
  wp_enqueue_script('html2canvas');
  wp_enqueue_script('jquery_plugin_html2canvas');
  ?>
  <div class="light-box">
    <div class="content-light-box">

      <div class="header-light-box">
        <img class="hide-light-box" src="<?php echo plugins_url( '/img/ic_close_black_24px.svg', __FILE__ ); ?>" alt="Close" />
        <h3><?php _e( 'Add New Ticket ', 'support' ); ?></h3>
      </div>

      <div class="body-light-box">
        <form class="create-ticket" action="" method="post" id="create-ticket">
          <div class="form-group">
            <label class="title-of-problem" for="title-of-problem"><?php _e( 'Title ', 'support' ); ?></label>
            <input class="form-ticket" id='title-of-problem' type="text" name="title-of-problem">
          </div>

          <div class="form-group">
            <label class="description-of-problem" for="description-of-problem"><?php _e( 'Description ', 'support' ); ?></label>
            <textarea id='description' class="form-ticket" name="description-of-problem" type="text" rows="4"></textarea>
          </div>

          <input id='url-of-the-problem-page' class="form-ticket" type="hidden" name="url-of-the-problem-page" value="<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]; ?>">

          <div class="form-group">
            <label class="priority" for="priority"><?php _e( 'Priority ', 'support' ); ?></label>
            <select class="form-ticket" name="priority" id="ticket-priority">
              <option value="Low"><?php _e( 'Low', 'support' ); ?></option>
              <option value="Medium"><?php _e( 'Medium', 'support' ); ?></option>
              <option value="High"><?php _e( 'High', 'support' ); ?></option>
            </select>
          </div>

          <div class="group">
            <label class="browser" for="browser"><?php _e( 'The browser you are using is ', 'support' ); ?></label>
            <select class="form-ticket" name="browser" id="browser">
              <option value="not entered browser" <?php if ( 'not entered browser' == $browser ) echo 'selected'; ?>></option>
              <option value="Google Chrome" <?php if ( 'Google Chrome' == $browser ) echo 'selected'; ?>><?php _e( 'Google Chrome', 'support' ); ?></option>
              <option value="Mozilla Firefox" <?php if ( 'Mozilla Firefox' == $browser ) echo 'selected'; ?>><?php _e( 'Mozilla Firefox', 'support' ); ?></option>
              <option value="Opera" <?php if ( 'Opera' == $browser ) echo 'selected'; ?>><?php _e( 'Opera', 'support' ); ?></option>
              <option value="Internet Explorer" <?php if ( 'Internet Explorer' == $browser ) echo 'selected'; ?>><?php _e( 'Internet Explorer', 'support' ); ?></option>
              <option value="Microsoft Edge" <?php if ( 'Microsoft Edge' == $browser ) echo 'selected'; ?>><?php _e( 'Microsoft Edge', 'support' ); ?></option>
              <option value="Safari" <?php if ( 'Safari' == $browser ) echo 'selected'; ?>><?php _e( 'Safari', 'support' ); ?></option>
              <option value="Other" <?php if ( 'Other' == $browser ) echo 'selected'; ?>><?php _e( 'Other', 'support' ); ?></option>
            </select>
          </div>

          <input type="hidden" name="screenshot" id="screenshot" value="">
          <input type="hidden" name="id-upload-media" id="id-upload-media" value="">
          <div class="form-group"></div>
        </form>
        <button id="submit-ticket" name="submit-ticket" type="submit" form="create-ticket" value="Submit"><?php _e( 'Submit', 'support' );?></button>
      </div>

	  <div class="notification-form-ticket">
		<p><?php _e( '*After pressing the submit button please wait... Creating a screenshot. ', 'support' ); ?></p>
	  </div>

    </div>

    <div class="black_overlay"></div>

  </div>

  <script type="text/javascript">
  function createScreenshot() {
    html2canvas(document.body, {
      onrendered: function(canvas) {
        jQuery("#screenshot").val(canvas.toDataURL("image/png"));
        if ( jQuery( "#screenshot" ).val() ) {
          jQuery.ajax({
            url: '<?php echo plugins_url( 'upload-screenshot.php', __FILE__ ); ?>',
            data: {
              'screenshot': jQuery("#screenshot").val(),
            },
            type: 'POST',
            success: function(response) {
              jQuery("#id-upload-img").val(response);
              jQuery.ajax({
                url: '<?php echo plugins_url( 'create-ticket-from-admin-bar.php', __FILE__ ); ?>',
                data: {
                  'title-of-problem': jQuery("#title-of-problem").val(),
                  'description': jQuery("#description").val(),
                  'url-of-the-problem-page': jQuery("#url-of-the-problem-page").val(),
                  'priority': jQuery("#ticket-priority").val(),
                  'browser': jQuery("#browser").val(),
                  'id-upload-media': response
                },
                type: 'POST',
                success: function() {
                  jQuery('.create-ticket').submit();
                }
              });
            }
          });
        }
      }
    });
  }

  jQuery(document).ready(function($) {
      $( "#submit-ticket" ).click(function( event ){
      $( "#submit-ticket" ).attr( "disabled", '');
      event.preventDefault();
      $(".light-box").hide();
      createScreenshot();
      $(".light-box").show();
    });
  });
  </script>
  <?php
}

add_action('in_admin_footer', 'create_ticket_admin_bar_ticket');
add_action('wp_footer', 'create_ticket_admin_bar_ticket');
?>
