<?php
add_filter( 'manage_support_posts_columns', 'set_custom_edit_ticket_columns' );
add_action( 'manage_support_posts_custom_column' , 'custom_ticket_column', 10, 2 );

function set_custom_edit_ticket_columns($columns) {
  $columns['priority_ticket'] = __( 'Priority', 'support' );
  $columns['status_ticket'] = __( 'Status', 'support' );

  return $columns;
}

function custom_ticket_column($column, $post_id) {
  switch ( $column ) {
    case 'priority_ticket' :
      echo get_post_meta( $post_id, '_priority_meta_value_key', true );
      break;

    case 'status_ticket' :
      $status = get_post_meta( $post_id, '_status_meta_value_key', true );
      if ($status == 1) {
        echo 'Solved';
      } else {
        echo 'Not Solved';
      }
      break;
  }
}
?>
