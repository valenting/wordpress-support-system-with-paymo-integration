# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-05-26 21:13+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: add-custom-column-tickets-list.php:6
msgid "Priority"
msgstr ""

#: add-custom-column-tickets-list.php:7
msgid "Status"
msgstr ""

#: add-meta-boxes.php:6
msgid "More for ticket"
msgstr ""

#: add-meta-boxes.php:13
msgid "Media"
msgstr ""

#: add-meta-boxes.php:27
msgid "URL of the problem page "
msgstr ""

#: add-meta-boxes.php:29
msgid "Link"
msgstr ""

#: add-meta-boxes.php:33 form-create-ticket-admin-bar.php:31
msgid "Priority "
msgstr ""

#: add-meta-boxes.php:35 form-create-ticket-admin-bar.php:33
msgid "Low"
msgstr ""

#: add-meta-boxes.php:36 form-create-ticket-admin-bar.php:34
msgid "Medium"
msgstr ""

#: add-meta-boxes.php:37 form-create-ticket-admin-bar.php:35
msgid "High"
msgstr ""

#: add-meta-boxes.php:42 form-create-ticket-admin-bar.php:40
msgid "The browser you are using is "
msgstr ""

#: add-meta-boxes.php:45 form-create-ticket-admin-bar.php:43
msgid "Google Chrome"
msgstr ""

#: add-meta-boxes.php:46 form-create-ticket-admin-bar.php:44
msgid "Mozilla Firefox"
msgstr ""

#: add-meta-boxes.php:47 form-create-ticket-admin-bar.php:45
msgid "Opera"
msgstr ""

#: add-meta-boxes.php:48 form-create-ticket-admin-bar.php:46
msgid "Internet Explorer"
msgstr ""

#: add-meta-boxes.php:49 form-create-ticket-admin-bar.php:47
msgid "Microsoft Edge"
msgstr ""

#: add-meta-boxes.php:50 form-create-ticket-admin-bar.php:48
msgid "Safari"
msgstr ""

#: add-meta-boxes.php:51 form-create-ticket-admin-bar.php:49
msgid "Other"
msgstr ""

#: add-meta-boxes.php:56
msgid "Status "
msgstr ""

#: add-meta-boxes.php:58
msgid "The problem is not solved"
msgstr ""

#: add-meta-boxes.php:59
msgid "The problem is solved"
msgstr ""

#: add-meta-boxes.php:64
msgid "Media input "
msgstr ""

#: add-meta-boxes.php:65
msgid "Add media "
msgstr ""

#: add-settings-page.php:5 add-settings-page.php:6 add-settings-page.php:42
msgid "Settings"
msgstr ""

#: add-settings-page.php:14
msgid "Paymo Integration"
msgstr ""

#: add-settings-page.php:52
msgid "Check if you want integration with Paymo."
msgstr ""

#: add-settings-page.php:75
msgid "Check if you want email notification."
msgstr ""

#: add-settings-page.php:83
msgid "Save Changes"
msgstr ""

#: admin-bar-ticket.php:6 create-custom-post-type-support.php:9
msgid "Add New Ticket"
msgstr ""

#: create-custom-post-type-support.php:4
msgctxt "post type general name"
msgid "Support"
msgstr ""

#: create-custom-post-type-support.php:5
msgctxt "post type singular name"
msgid "Support"
msgstr ""

#: create-custom-post-type-support.php:6
msgctxt "admin menu"
msgid "Support"
msgstr ""

#: create-custom-post-type-support.php:7
msgctxt "add new on admin bar"
msgid "Ticket"
msgstr ""

#: create-custom-post-type-support.php:8
msgctxt "ticket"
msgid "Add New"
msgstr ""

#: create-custom-post-type-support.php:10
msgid "New Ticket"
msgstr ""

#: create-custom-post-type-support.php:11
msgid "Edit Ticket"
msgstr ""

#: create-custom-post-type-support.php:12
msgid "View Ticket"
msgstr ""

#: create-custom-post-type-support.php:13
msgid "All Tickets"
msgstr ""

#: create-custom-post-type-support.php:14
msgid "Search Ticket"
msgstr ""

#: create-custom-post-type-support.php:15
msgid "Parent Tickets:"
msgstr ""

#: create-custom-post-type-support.php:16
msgid "No tickets found."
msgstr ""

#: create-custom-post-type-support.php:17
msgid "No tickets found in Trash."
msgstr ""

#: create-custom-post-type-support.php:22
msgid "Description."
msgstr ""

#: form-create-ticket-admin-bar.php:13
msgid "Add New Ticket "
msgstr ""

#: form-create-ticket-admin-bar.php:19
msgid "Title "
msgstr ""

#: form-create-ticket-admin-bar.php:24
msgid "Description "
msgstr ""

#: form-create-ticket-admin-bar.php:57
msgid "Submit"
msgstr ""

#: form-create-ticket-admin-bar.php:61
msgid ""
"*After pressing the submit button please wait... Creating a screenshot. "
msgstr ""
