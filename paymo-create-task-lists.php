<?php
$api = get_option( 'paymo-input-api' );
$projectname = get_option( 'paymo-input-projectname' );

if( ($api != '') && ($projectname != '') ) {
  $get_args = array(
    'timeout'     => 5,
    'redirection' => 5,
    'httpversion' => '1.0',
    'user-agent'  => 'WordPress/' . $wp_version . '; ' . get_bloginfo( 'url' ),
    'blocking'    => true,
    'headers'     => array('Authorization' => 'Basic ' . base64_encode($api . ':' . 'X')),
    'cookies'     => array(),
    'body'        => array(),
    'compress'    => false,
    'decompress'  => true,
    'sslverify'   => true,
    'stream'      => false,
    'filename'    => null
  );

  //Getting projects
  $url_projects = 'https://app.paymoapp.com/api/projects';
  $response_projects = wp_remote_get($url_projects, $get_args);
  $get_projects = json_decode($response_projects['body']);
  //--------------------------------------------------------------------------

  foreach ($get_projects->projects as $project) {
    if($projectname == $project->name) {
      $projectid = $project->id;
    }
  }

  //Getting task lists
  $url_tasklists = 'https://app.paymoapp.com/api/tasklists?where=project_id=' . $projectid;
  $response_tasklists = wp_remote_get($url_tasklists, $get_args);
  $get_task_id_object = json_decode($response_tasklists['body']);
  //----------------------------------------------------------------------------

  function creeate_tasklist($projectid, $tasklist_name, $api) {
    $args_tasklist = array(
      'method' => 'POST',
      'timeout' => 45,
      'redirection' => 5,
      'httpversion' => '1.0',
      'blocking' => true,
      'headers' => array('Authorization' => 'Basic ' . base64_encode($api . ':' . 'X')),
      'body' => array('name' => $tasklist_name, 'project_id' => $projectid),
      'cookies' => array()
    );
    wp_remote_post($GLOBALS['url_tasklists'] , $args_tasklist);
  }

  $have_tasklist_low = 0;
  $have_tasklist_medium = 0;
  $have_tasklist_high = 0;

  foreach ($get_task_id_object->tasklists as $tasklist) {
    if($tasklist->name == 'Low') {
      $have_tasklist_low = 1;
    }

    if($tasklist->name == 'Medium') {
      $have_tasklist_medium = 1;
    }

    if($tasklist->name == 'High') {
      $have_tasklist_high = 1;
    }
  }

  if ($have_tasklist_low != 1) {
    $tasklist_name = 'Low';
    creeate_tasklist($projectid, $tasklist_name, $api);
  }

  if ($have_tasklist_medium != 1) {
    $tasklist_name = 'Medium';
    creeate_tasklist($projectid, $tasklist_name, $api);
  }

  if ($have_tasklist_high != 1) {
    $tasklist_name = 'High';
    creeate_tasklist($projectid, $tasklist_name, $api);
  }
}
?>
