<?php
function paymo_delete_tasks( $post_id ) {
  $api = get_option( 'paymo-input-api' );
  $task_id = get_post_meta( $post_id, 'task_id', true );
  $args_comment = array(
    'method' => 'POST',
    'timeout' => 45,
    'redirection' => 5,
    'httpversion' => '1.0',
    'blocking' => true,
    'headers' => array('Authorization' => 'Basic ' . base64_encode($api . ':' . 'X')),
    'body' => array('task_id' => $task_id, 'content' =>
                    '<p style="color:red;">The ticket is deleted from WordPress.</p>'),
    'cookies' => array()
  );
  $url_comments = 'https://app.paymoapp.com/api/comments';
  $comment =  wp_remote_post( $url_comments, $args_comment);
}
add_action( 'wp_trash_post', 'paymo_delete_tasks');
?>
