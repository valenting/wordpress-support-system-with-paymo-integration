<?php
add_action( 'trash_comment', 'paymo_delete_comments');
function paymo_delete_comments( $comment_ID) {
  $api = get_option( 'paymo-input-api' );
  $comment = get_comment( $comment_ID );
  $wp_commet_content = $comment->comment_content;
  $paymo_wp_comment_id = get_comment_meta( $comment->comment_ID, 'comment_id', true );

  $args_comment = array(
    'method' => 'POST',
    'timeout' => 45,
    'redirection' => 5,
    'httpversion' => '1.0',
    'blocking' => true,
    'headers' => array('Authorization' => 'Basic ' . base64_encode($api . ':' . 'X')),
    'body' => array('content' => $wp_commet_content . '<br>'.
                    '<p style="color:red;"> This comment was deleted by WordPress.</p>'),
    'cookies' => array()
  );

  $url_comments = 'https://app.paymoapp.com/api/comments/' . $paymo_wp_comment_id;
  wp_remote_post( $url_comments, $args_comment);
}
?>
