<?php
add_action( 'add_meta_boxes', 'ticket_add_custom_box' );
function ticket_add_custom_box() {
  add_meta_box(
    'more_for_ticket_box_id',
    __( 'More for ticket', 'support' ),
    'more_for_ticket_meta_box_callback',
    'support'
  );

  add_meta_box(
    'media_box_id',
    __( 'Media', 'support' ),
    'media_meta_box_callback',
    'support'
  );
}

function more_for_ticket_meta_box_callback( $post ) {
  wp_enqueue_script('js_upload_media_from_back_end');
  $priority = get_post_meta( $post->ID, '_priority_meta_value_key', true );
  $url_of_the_problem_page = get_post_meta( $post->ID, '_url_meta_value_key', true );
  $status = get_post_meta( $post->ID, '_status_meta_value_key', true );
  $browser = get_post_meta( $post->ID, '_browser', true );
  ?>
  <div class="group">
    <label class="url-of-the-problem-page" for="url-of-the-problem-page"><?php _e( 'URL of the problem page ', 'support' ); ?></label>
    <input class="form-ticket" type="text" name="url-of-the-problem-page" value="<?php echo $url_of_the_problem_page; ?>">
    <a class="button" href="<?php echo $url_of_the_problem_page; ?>" role="button" target="_blank"><?php _e( 'Link', 'support' ); ?></a>
  </div>

  <div class="group">
    <label class="priority-back-end" for="priority-back-end"><?php _e( 'Priority ', 'support' ); ?></label>
    <select class="form-ticket" name="priority" id="priority">
      <option value="Low" <?php if ( 'Low' == $priority ) echo 'selected'; ?>><?php _e( 'Low', 'support' ); ?></option>
      <option value="Medium" <?php if ( 'Medium' == $priority ) echo 'selected'; ?>><?php _e( 'Medium', 'support' ); ?></option>
      <option value="High" <?php if ( 'High' == $priority ) echo 'selected'; ?>><?php _e( 'High', 'support' ); ?></option>
    </select>
  </div>

 <div class="group">
   <label class="browser" for="browser"><?php _e( 'The browser you are using is ', 'support' ); ?></label>
   <select class="form-ticket" name="browser" id="browser">
     <option value="not entered browser" <?php if ( 'not entered browser' == $browser ) echo 'selected'; ?>></option>
     <option value="Google Chrome" <?php if ( 'Google Chrome' == $browser ) echo 'selected'; ?>><?php _e( 'Google Chrome', 'support' ); ?></option>
     <option value="Mozilla Firefox" <?php if ( 'Mozilla Firefox' == $browser ) echo 'selected'; ?>><?php _e( 'Mozilla Firefox', 'support' ); ?></option>
     <option value="Opera" <?php if ( 'Opera' == $browser ) echo 'selected'; ?>><?php _e( 'Opera', 'support' ); ?></option>
     <option value="Internet Explorer" <?php if ( 'Internet Explorer' == $browser ) echo 'selected'; ?>><?php _e( 'Internet Explorer', 'support' ); ?></option>
     <option value="Microsoft Edge" <?php if ( 'Microsoft Edge' == $browser ) echo 'selected'; ?>><?php _e( 'Microsoft Edge', 'support' ); ?></option>
     <option value="Safari" <?php if ( 'Safari' == $browser ) echo 'selected'; ?>><?php _e( 'Safari', 'support' ); ?></option>
     <option value="Other"<?php if ( 'Other' == $browser ) echo 'selected'; ?>><?php _e( 'Other', 'support' ); ?></option>
   </select>
 </div>

 <div class="group">
   <label class="status" for="status"><?php _e( 'Status ', 'support' ); ?></label>
   <select class="form-ticket" name="status" id="status">
     <option value="0" <?php if ( '0' == $status ) echo 'selected'; ?>><?php _e( 'The problem is not solved', 'support' ); ?></option>
     <option value="1" <?php if ( '1' == $status ) echo 'selected'; ?>><?php _e( 'The problem is solved', 'support' ); ?></option>
   </select>
 </div>

  <div class="group">
    <label class="media-input" for="status"><?php _e( 'Media input ', 'support' ); ?></label>
    <button class="button" id="upload-media" type="button"><?php _e( 'Add media ', 'support' ); ?></button>
  </div>

  <?php
}

function media_meta_box_callback( $post ) {
  wp_enqueue_script('js_upload_media_from_back_end');

  echo '<div class="before-save-post">';
    echo '<input type="hidden" name="id-upload-media" id="id-media-upload" value="">';
  echo '</div>';

  $ids_upload_media = get_post_meta( $post->ID, '_id_upload_media_meta_value_key', true);

  if(!empty($ids_upload_media)) {
    $media_ids = explode(',', $ids_upload_media);
  }

  $reverse_media_ids = array_reverse($media_ids);
  echo '<ul class="media-list">';
  foreach($reverse_media_ids as $id_media) {
    $media = get_post( $id_media);

    switch ($media->post_mime_type) {
      case 'image/jpg':
      case 'image/jpeg':
      case 'image/png':
      case 'image/gif':
      case 'image/ico':
      case 'image/svg':
        echo '<li>';
          echo '<div class="media-thumbnail">';
            echo '<a href="'. $media->guid .'" target="_blank">';
              echo'<div class="media-img-image">';
                echo '<img src="'. wp_get_attachment_image_url( $id_media, $size = 'thumbnail' ) .'" alt="" />';
              echo '</div>';
              echo '<span class="media-name"><span>'. $media->post_title .'</span></span>';
            echo '</a>';
          echo'</div>';
        echo '</li>';
        break;
      case 'video/mp4':
      case 'video/m4v':
      case 'video/mov':
      case 'video/wmv':
      case 'video/avi':
      case 'video/mpg':
      case 'video/ogv':
      case 'video/3gp':
      case 'video/3g2':
        echo '<li>';
          echo '<div class="media-thumbnail">';
            echo '<a href="'.  get_edit_post_link( $id_media ) .'" target="_blank">';
              echo'<div class="media-img">';
                echo '<img src="'. plugins_url("img/video.png",__FILE__). '" alt="" />';
              echo '</div>';
              echo '<span class="media-name"><span>'. $media->post_title .'</span></span>';
            echo '</a>';
          echo'</div>';
        echo '</li>';
        break;
      case 'text/csv':
      case 'text/plain':
      case 'text/xml':
        echo '<li>';
          echo '<div class="media-thumbnail">';
            echo '<a href="'. $media->guid .'" target="_blank">';
              echo'<div class="media-img">';
                echo '<img src="'. plugins_url("/img/text.png",__FILE__). '" alt="" />';
              echo '</div>';
              echo '<span class="media-name"><span>'. $media->post_title .'</span></span>';
            echo '</a>';
          echo'</div>';
        echo '</li>';
        break;
      case 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
      case 'application/vnd.ms-powerpoint':
      case 'application/vnd.openxmlformats-officedocument.presentationml.slideshow':
      case 'application/pdf':
      case 'application/vnd.oasis.opendocument.text':
      case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
      case 'application/msword':
      case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
      case 'application/vnd.ms-excel':
        echo '<li>';
          echo '<div class="media-thumbnail media" >';
            echo '<a href="'. $media->guid .'" target="_blank">';
              echo'<div class="media-img">';
                echo '<img src="'. plugins_url("img/application.png",__FILE__). '" alt="" />';
              echo '</div>';
              echo '<span class="media-name"><span>'. $media->post_title .'</span></span>';
            echo '</a>';
          echo'</div>';
        echo '</li>';
        break;
      case 'audio/mp3':
      case 'audio/m4a':
      case 'audio/ogg':
      case 'audio/wav':
      case 'audio/mpeg':
        echo '<li>';
          echo '<div class="media-thumbnail">';
            echo '<a href="'.  get_edit_post_link( $id_media ) .'" target="_blank">';
              echo'<div class="media-img">';
                echo '<img src="'. plugins_url("img/audio.png",__FILE__). '" alt="" />';
              echo '</div>';
              echo '<span class="media-name"><span>'. $media->post_title .'</span></span>';
            echo '</a>';
          echo'</div>';
        echo '</li>';
        break;
    }
  }
  echo '</ul>';
}


add_action( 'save_post', 'ticket_save_postdata' );
function ticket_save_postdata( $post_id ) {
  if ( array_key_exists('priority', $_POST ) ) {
    update_post_meta( $post_id,
      '_priority_meta_value_key',
      $_POST['priority']
    );
  }

  if ( array_key_exists('url-of-the-problem-page', $_POST ) ) {
    update_post_meta( $post_id,
      '_url_meta_value_key',
      $_POST['url-of-the-problem-page']
    );
  }

  if ( array_key_exists('status', $_POST ) ) {
    update_post_meta( $post_id,
      '_status_meta_value_key',
      $_POST['status']
    );
  }

  if ( array_key_exists('browser', $_POST ) ) {
    update_post_meta( $post_id,
      '_browser',
      $_POST['browser']
    );
  }

  $ids_upload_media = get_post_meta( $post_id, '_id_upload_media_meta_value_key');
  if(!empty($ids_upload_media)) {
    array_push($ids_upload_media, $_POST['id-upload-media']);
    $new_id = implode(",", $ids_upload_media);
    update_post_meta( $post_id,
      '_id_upload_media_meta_value_key',
      $new_id
    );
  } elseif ($_POST['id-upload-media'] ) {
    update_post_meta($post_id,
      '_id_upload_media_meta_value_key',
      $_POST['id-upload-media']
    );
  }
}
?>
