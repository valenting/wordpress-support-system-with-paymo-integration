<?php
add_action( 'admin_menu', 'settings_page_ticket' );
function settings_page_ticket() {
  add_submenu_page('edit.php?post_type=support',
                    __('Settings','support'),
                    __('Settings','support'),
                    'edit_posts',
                    'ticketsettings',
                    'ticket_settings_page');
}

function ticket_settings_page_init() {
  add_settings_section("paymo_settings_section", null, null, "ticketsettings");
  add_settings_field("paymo-checkbox", __( 'Paymo Integration', 'support' ), "paymo_checkbox_display", "ticketsettings", "paymo_settings_section");
  register_setting("paymo_settings_section", "paymo-checkbox");
  add_settings_field("paymo-input-api", null, "paymo_input_api_display", "ticketsettings", "paymo_settings_section");
  register_setting("paymo_settings_section", "paymo-input-api");
  add_settings_field("paymo-input-projectname", null, "paymo_input_projectname_display", "ticketsettings", "paymo_settings_section");
  register_setting("paymo_settings_section", "paymo-input-projectname");
}
add_action("admin_init", "ticket_settings_page_init");

function ticket_settings_page() {
  wp_enqueue_script('jQuery');

  $user_ID = get_current_user_id();
  $options_api = get_option( 'paymo-input-api' );
  $options_projectname = get_option( 'paymo-input-projectname' );

  $user = get_userdata($user_ID);
  $user_role = implode(', ', $user->roles);

  if (isset($_POST['submit'])) {
    $email_input_checkbox = $_POST['email-checkbox'];
    update_user_meta( $user_ID, '_email_input_checkbox', $email_input_checkbox);
  }

  ?>

  <div class="wrap">

    <h2><?php _e('Settings', 'support'); ?></h2>

    <form method="post">
      <table class="form-table">
        <tbody>

          <tr>
            <th scope="row">Paymo Integration</th>
            <td>
              <input id='paymo-checkbox' type="checkbox" name="paymo-checkbox" value="1" <?php checked(1, get_option('paymo-checkbox'), true); ?> />
              <label class="paymo-checkbox-explanation" for="paymo-checkbox"><?php _e( 'Check if you want integration with Paymo.', 'support' ); ?></label>
            </td>
          </tr>

          <tr>
            <th scope="row">Paymo API Key</th>
            <td>
              <input id="paymo-input-api" type='password' name='paymo-input-api' value='<?php echo $options_api; ?>'>
            </td>
          </tr>

          <tr>
            <th scope="row">Paymo Project Name</th>
            <td>
              <input id="paymo-input-projectname" type='text' name='paymo-input-projectname' value='<?php echo $options_projectname; ?>'>
            </td>
          </tr>

          <?php if($user_role != 'administrator'){ ?>
            <tr>
              <th scope="row">Email notification</th>
              <td>
                <input id='email-checkbox' type="checkbox" name="email-checkbox" value="1" <?php checked(1, get_user_meta( $user_ID, '_email_input_checkbox', true ), true); ?> />
                <label class="email-checkbox-explanation" for="email-checkbox"><?php _e( 'Check if you want email notification.', 'support' ); ?></label>
              </td>
            </tr>
          <?php } ?>

        </tbody>
      </table>
      <p class="submit">
        <input type="submit" name="submit" id="submit" class="button button-primary" value="<?php _e( 'Save Changes', 'support' );?>">
      </p>
    </form>
  </div>
  <?php
}

if (isset($_POST['submit'])) {
  update_option( 'paymo-checkbox', $_POST['paymo-checkbox'] );
  update_option( 'paymo-input-api', $_POST['paymo-input-api'] );
  update_option( 'paymo-input-projectname', $_POST['paymo-input-projectname'] );
}
?>
