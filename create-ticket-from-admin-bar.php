<?php
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );

$post_title = $_POST['title-of-problem'];
$post_content = $_POST['description'];

$new_post = array(
  'post_content' => $post_content,
  'post_title' => $post_title,
  'post_status' => 'publish',
  'post_type' => 'support'
);

$post_id = wp_insert_post($new_post);

update_post_meta($post_id, 'url', $_POST['url-of-the-problem-page']);
update_post_meta($post_id, 'priority', $_POST['priority']);
update_post_meta($post_id, 'browser', $_POST['browser']);
update_post_meta($post_id, 'img', $_POST['id-upload-media']);
?>
