<?php
function paymo_edit_comments( $comment_ID) {
  $api = get_option( 'paymo-input-api' );
  $projectname = get_option( 'paymo-input-projectname' );
  $get_args = array(
    'timeout'     => 5,
    'redirection' => 5,
    'httpversion' => '1.0',
    'user-agent'  => 'WordPress/' . $wp_version . '; ' . get_bloginfo( 'url' ),
    'blocking'    => true,
    'headers'     => array('Authorization' => 'Basic ' . base64_encode($api . ':' . 'X')),
    'cookies'     => array(),
    'body'        => array(),
    'compress'    => false,
    'decompress'  => true,
    'sslverify'   => true,
    'stream'      => false,
    'filename'    => null
  );

  //Getting projects
  $url_projects = 'https://app.paymoapp.com/api/projects';
  $response_projects = wp_remote_get($url_projects, $get_args);
  $get_projects = json_decode($response_projects['body']);
  //--------------------------------------------------------------------------

  foreach ($get_projects->projects as $project) {
    if($projectname == $project->name){
      $project_id = $project->id;
    }
  }

  $comment = get_comment( $comment_ID );
  $post_id = $comment->comment_post_ID;
  $wp_commet_content = $comment->comment_content;

  $task_id = get_post_meta( $post_id, 'task_id', true );
  $paymo_wp_comment_id = get_comment_meta( $comment->comment_ID, 'comment_id', true );

  //Paymo Getting comments
  $url_comments_id = "https://app.paymoapp.com/api/threads?where=task_id=" . "$task_id" . "&include=comments";
  $paymo_response_comments = wp_remote_get($url_comments_id, $get_args);
  $paymo_get_comments = json_decode($paymo_response_comments['body']);
  //end get paymo comments

  foreach ($paymo_get_comments->threads as $paymo_thread) {
    foreach ($paymo_thread->comments as $paymo_content) {

      if($paymo_content->id == $paymo_wp_comment_id) {
        if($paymo_content->content != $wp_commet_content){
          $comment_comment = $wp_commet_content;
        }
      }
        $args_comment = array(
          'method' => 'POST',
          'timeout' => 45,
          'redirection' => 5,
          'httpversion' => '1.0',
          'blocking' => true,
          'headers' => array('Authorization' => 'Basic ' . base64_encode($api . ':' . 'X')),
          'body' => array('content' => $comment_comment),
          'cookies' => array()
        );

        $url_comments = 'https://app.paymoapp.com/api/comments/' . $paymo_content->id;
        wp_remote_post( $url_comments, $args_comment);

    }
  }
}
add_action( 'edit_comment', 'paymo_edit_comments');
?>
