<?php
function create_custom_post_type_support() {
  $labels = array(
    'name'               => _x( 'Support', 'post type general name', 'support' ),
    'singular_name'      => _x( 'Support', 'post type singular name', 'support' ),
    'menu_name'          => _x( 'Support', 'admin menu', 'support' ),
    'name_admin_bar'     => _x( 'Ticket', 'add new on admin bar', 'support' ),
    'add_new'            => _x( 'Add New', 'ticket', 'support' ),
    'add_new_item'       => __( 'Add New Ticket', 'support' ),
    'new_item'           => __( 'New Ticket', 'support' ),
    'edit_item'          => __( 'Edit Ticket', 'support' ),
    'view_item'          => __( 'View Ticket', 'support' ),
    'all_items'          => __( 'All Tickets', 'support' ),
    'search_items'       => __( 'Search Ticket', 'support' ),
    'parent_item_colon'  => __( 'Parent Tickets:', 'support' ),
    'not_found'          => __( 'No tickets found.', 'support' ),
    'not_found_in_trash' => __( 'No tickets found in Trash.', 'support' )
  );

  $args = array(
    'labels'             => $labels,
    'description'        => __( 'Description.', 'support' ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'support' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => null,
    'supports'           => array( 'title', 'editor', 'comments', 'author')
  );

  register_post_type( 'support', $args );
}
add_action( 'init', 'create_custom_post_type_support' );
?>
